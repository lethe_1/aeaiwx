package com.agileai.weixin.controller.webtool;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.weixin.common.HttpClientHelper;
import com.agileai.weixin.model.Constans.Configs;

public class WeatherForecastHandler extends SimpleHandler{
	public WeatherForecastHandler(){
		super();
	}
	
	public ViewRenderer prepareDisplay(DataParam param) {
		try {
			String location = param.get("location");
			String url = "http://api.map.baidu.com/telematics/v3/weather?location="+location+"&output=json&ak="+Configs.BAIDU_KEY;
			System.out.println("url is " + url);
			HttpClientHelper httpClientHelper = new HttpClientHelper();
	        String jsonStr = httpClientHelper.retrieveGetReponsetText(url);
	        JSONObject jsonObject = new JSONObject(jsonStr);
	        if ("success".equals(jsonObject.getString("status"))){
	        	JSONArray jsonArray = jsonObject.getJSONArray("results");
	        	if (jsonArray.length() == 1){
	        		JSONObject content = (JSONObject)jsonArray.get(0);
	        		String currentCity = content.getString("currentCity");
	        		String pm25 = content.getString("pm25");
	        		
	        		this.setAttribute("currentCity", currentCity);
	        		this.setAttribute("pm25", pm25);
	        		
	        		JSONArray weatherDatas = content.getJSONArray("weather_data");
	        		List<DataRow> records = new ArrayList<DataRow>();
	        		boolean isOnDay = this.isOnDay();
	        		for (int i=0;i < weatherDatas.length();i++){
	        			DataRow row = new DataRow();
	        			JSONObject weatherData = weatherDatas.getJSONObject(i);
	        			row.put("date",weatherData.getString("date"));
	        			if (isOnDay){
	        				row.put("weatherPictureUrl",weatherData.getString("dayPictureUrl"));	
	        			}else{
	        				row.put("weatherPictureUrl",weatherData.getString("nightPictureUrl"));		        				
	        			}
	        			row.put("weather",weatherData.getString("weather"));
	        			row.put("wind",weatherData.getString("wind"));
	        			row.put("temperature",weatherData.getString("temperature"));
	        			records.add(row);
	        		}
	        		this.setRsList(records);
	        	}
	        }else{
	        	this.setErrorMsg("获取天气预报数据失败！");
	        }
		}
		catch(Exception ex){
			log.error(ex.getLocalizedMessage(), ex);
		}
		return new LocalRenderer(getPage());
	}
	
	private boolean isOnDay(){
		int hour = DateUtil.getHour();
		return (hour >=6 && hour < 18); 
	}
}
