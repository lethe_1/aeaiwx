package com.agileai.weixin.controller.webtool;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.SimpleHandler;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class CurrentLocationHandler extends SimpleHandler{
	public CurrentLocationHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		return new LocalRenderer(getPage());
	}
}